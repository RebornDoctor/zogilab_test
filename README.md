# ZogiLab_Test



## Getting started

To make it easy for you to get started with ZogiLab test App, here's a list of recommended next steps.
open your terminal:

*  npm run install
*  npm run dev
*  open the port that the app is currently running at!


## About

this is an app that connect to your metamask wallet and show your balance and on which network your currently at,
there is a listeners that will update the balance whenever you choose different account or different network, however
if you are not connected to the wallet the listeners wont work until you getStarted also obv errors are handled as well!


Enjoy!